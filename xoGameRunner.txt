package XOGame;

public class XOGameRunner
{

    public static void main(String[] args)
    {
        XOGame game = new XOGame();
        game.intro();
        game.entCont();

        while (!game.isOver)
        {
            game.setPlayer1Name();
            game.setPlayer2Name();
            game.isOver = game.updateBoard(game.getPlayer1Name(), game.getPlayer2Name(),
                    game.printStartBoard(), game.isOver);

        }

    }

}
